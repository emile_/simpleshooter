from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers.core import Dense, Dropout
import random
import numpy as np
import pandas as pd
from operator import add


class DQNAgent(object):

    def __init__(self):
        self.reward = 0
        self.gamma = 0.9
        self.dataframe = pd.DataFrame()
        self.short_memory = np.array([])
        self.agent_target = 1
        self.agent_predict = 0
        self.learning_rate = 0.0005
        self.model = self.network()
        #self.model = self.network("weights.hdf5")
        self.epsilon = 0
        self.actual = []
        self.memory = []
        self.state_dim = 9

    def get_state(self, score, player, enemies):
        closest = None
        if len(enemies) > 0:
            closest = max(enemies, key = lambda e: e.y)
        state = [
            player.x > player.sw / 2, # player right of middle of screen
            player.x < player.sw / 2, # player left of middle of screen
            player.dx > 0, # moving right
            player.dx < 0, # moving left
            len(enemies) > 0 and all(e.x + e.ww > player.x and e.x - e.ww <= player.x and e.y < player.y-10 for e in enemies), # currently in front of enemy
            closest != None and closest.x + closest.ww < player.x, # closest enemy is left of player
            closest != None and closest.x - closest.ww > player.x, # closest enemy is right of player
            closest != None and closest.y + closest.hh > player.y - player.h * 6, # closest enemy is somewhat close to player
            closest != None and closest.y + closest.hh > player.y - player.h * 2 # closest enemy is very close to player
            ]

        for i in range(len(state)):
            if state[i]:
                state[i]=1
            else:
                state[i]=0

        return np.asarray(state)

    def set_reward(self, score_diff):
        self.reward = score_diff
        return self.reward

    def network(self, weights=None):
        model = Sequential()
        model.add(Dense(output_dim=120, activation='relu', input_dim=9))
        model.add(Dropout(0.15))
        model.add(Dense(output_dim=120, activation='relu'))
        model.add(Dropout(0.15))
        model.add(Dense(output_dim=120, activation='relu'))
        model.add(Dropout(0.15))
        model.add(Dense(output_dim=4, activation='softmax'))
        opt = Adam(self.learning_rate)
        model.compile(loss='mse', optimizer=opt)

        if weights:
            model.load_weights(weights)
        return model

    def remember(self, state, action, reward, next_state, score):
        self.memory.append((state, action, reward, next_state, score))

    def replay_new(self, memory):
        if len(memory) > 1000:
            minibatch = random.sample(memory, 1000)
        else:
            minibatch = memory
        for state, action, reward, next_state, score in minibatch:
            target = reward
            # target = reward + self.gamma * np.amax(self.model.predict(np.array([next_state]))[0])
            target_f = self.model.predict(np.array([state]))
            target_f[0][np.argmax(action)] = target
            self.model.fit(np.array([state]), target_f, epochs=1, verbose=0)

    def train_short_memory(self, state, action, reward, next_state, score):
        # target = reward
        target = reward + self.gamma * np.amax(self.model.predict(next_state.reshape((1, self.state_dim)))[0])
        target_f = self.model.predict(state.reshape((1, self.state_dim)))
        target_f[0][np.argmax(action)] = target
        self.model.fit(state.reshape((1, self.state_dim)), target_f, epochs=1, verbose=0)


    def train_full_memory(self, memory):
        for state, action, reward, next_state, score in minibatch:
            target = reward
            target = reward + self.gamma * np.amax(self.model.predict(np.array([next_state]))[0])
            target_f = self.model.predict(np.array([state]))
            target_f[0][np.argmax(action)] = target
            self.model.fit(np.array([state]), target_f, epochs=1, verbose=0)
        

