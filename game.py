import pygame
from random import randint
from DQN import DQNAgent
import numpy as np
from keras.utils import to_categorical
import matplotlib.pyplot as plt
#import seaborn as sns
import numpy as np

BLACK = (0,0,0)
WHITE = (255,255,255)
BACKGROUND = BLACK
BLUE = (100,100,255)
GREEN = (80,255,100)
RED = (255,10,100)
SCREEN_HEIGTH = 500
SCREEN_WIDTH = 400

pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGTH))
pygame.display.set_caption('SimpleShooter')
done = False
score = 0
font = pygame.font.Font(pygame.font.get_default_font(), 12) 

clock = pygame.time.Clock()

class Base:
    def __init__(self, x, y, w, h, color):
        self.w = w
        self.h = h
        self.ww = self.w/2
        self.hh = self.h/2
        self.x = x - self.ww
        self.y = y - self.hh
        self.dy = 0.0
        self.dx = 0.0
        self.alive = True
        self.color = color

    def update(self):
        self.x += self.dx
        self.y += self.dy

    def draw(self, screen):
        pygame.draw.rect(screen, self.color, pygame.Rect(self.x-self.ww, self.y-self.hh, self.w, self.h))

    def intersects(self, other):
        return other.x + other.ww >= self.x - self.ww and other.x - other.ww <= self.x + self.ww and other.y - other.hh <= self.y + self.hh and other.y + other.hh >= self.y - self.hh

    def die(self):
        self.alive = False

class Enemy(Base):
    def __init__(self, x, y):
        super().__init__(x, y, 20, 20, RED)
        self.dy = 3.0

    def update(self, player):
        super().update()
        ds = 0
        if self.intersects(player):
            ds -= 14
            self.die()
        elif self.y > player.y:
            ds -= 3
            self.die()
        else:
            for bullet in player.bullets:
                if self.intersects(bullet):
                    self.die()
                    bullet.die()
                    ds += 2
                    break
        return ds

class Bullet(Base):
    def __init__(self, x, y):
        super().__init__(x, y, 3, 10, GREEN)
        self.dy = -12

class Player(Base):
    def __init__(self, screen_width, screen_height):
        super().__init__(screen_width/2.0, screen_height-30, 30, 30, BLUE)
        self.sw = screen_width 
        self.speed = 9.0
        self.bullets = []
            
    def draw(self, screen):
        super().draw(screen)
        for bullet in self.bullets:
            bullet.draw(screen)

    def shoot(self):
        self.bullets.append(Bullet(self.x,self.y))

    def go_right(self):
        self.dx = self.speed

    def go_left(self):
        self.dx = -self.speed

    def stop_right(self):
        if self.dx == self.speed:
            self.dx = 0.0

    def stop_left(self):
        if self.dx == -self.speed:
            self.dx = 0.0

    def update(self):
        if (self.dx < 0 and self.x - self.ww > 0) or (self.dx > 0 and self.x + self.ww < self.sw):
            super().update()
        self.bullets = [b for b in self.bullets if b.alive and b.y > 0]
        for bullet in self.bullets:
            bullet.update()

p = Player(SCREEN_WIDTH, SCREEN_HEIGTH)
enemies = []
spawn_timer = 0
spawn_timer_max = 30
update_screen = True

def handle_events(p):
    global update_screen
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
                done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                p.go_right()
            if event.key == pygame.K_LEFT:
                p.go_left()
            if event.key == pygame.K_SPACE:
                p.shoot()
            if event.key == pygame.K_u:
                update_screen = not update_screen
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_RIGHT:
                p.stop_right()
            if event.key == pygame.K_LEFT:
                p.stop_left()

def handle_spawns(spawn_timer, spawn_timer_max, enemies):
    spawn_timer += 1
    if spawn_timer >= spawn_timer_max:
        spawn_timer = 0
        r = randint(20,SCREEN_WIDTH-4)
        enemies.append(Enemy(r, 0))
    return spawn_timer

def handle_updates(p, enemies, score):
    p.update()
    enemies = [e for e in enemies if e.alive]
    for enemy in enemies:
        score += enemy.update(p)

    return score, enemies

def handle_action(action, player):
    if action[0] == 1:
        player.shoot()
    elif action[1] == 1:
        player.go_right()
    elif action[2] == 1:
        player.go_left()
    elif action[3] == 1:
        player.stop_right()
        player.stop_left()

def init_game(player, score, enemies, agent):
    state_init1 = agent.get_state(score, player, enemies)  
    action = [0, 1, 0, 0]
    handle_action(action, player)
    state_init2 = agent.get_state(score, player, enemies)  
    reward1 = agent.set_reward(score)
    agent.remember(state_init1, action, reward1, state_init2, score)
    agent.replay_new(agent.memory)

agent = DQNAgent()
init_game(p, score, enemies, agent)
cgame = 1

while not done:
    if score < 0:
        agent.replay_new(agent.memory)
        score = 0
        enemies = []
        cgame += 1

    agent.epsilon = 80 - cgame # no random actions (exploration) after 80 games
    state_old = agent.get_state(score, p, enemies)
    old_score = score

    if randint(0, 200) < agent.epsilon:
        # to_categorical makes an array [0, 1, 0, 0] with a 1 at specified index 
        final_move = to_categorical(randint(0,3), num_classes=4)
    else:
        # predict action based on the old state
        prediction = agent.model.predict(state_old.reshape((1,agent.state_dim)))
        if update_screen:
            print(prediction[0])
        final_move = to_categorical(np.argmax(prediction[0]), num_classes=4)

    handle_action(final_move, p)

    handle_events(p)
    spawn_timer = handle_spawns(spawn_timer, spawn_timer_max, enemies)
    score, enemies = handle_updates(p, enemies, score)
    score_diff = score - old_score

    state_new = agent.get_state(score, p, enemies)
    reward = agent.set_reward(score_diff)
    agent.train_short_memory(state_old, final_move, reward, state_new, score)
    agent.remember(state_old, final_move, reward, state_new, score)

    if update_screen:
        screen.fill(BACKGROUND)
        p.draw(screen)
        for enemy in enemies:
            enemy.draw(screen)
        text = font.render('score: '+str(score), True, WHITE)
        text2 = font.render("game: "+str(cgame), True, WHITE)
        screen.blit(text, (5,5))
        screen.blit(text2, (5,20))

        pygame.display.flip()
        clock.tick(60)
