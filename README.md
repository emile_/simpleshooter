# SimpleShooter

A simple space shooter game made in pygame that will be used to learn Keras Deep Q-Learning.

## Installation
Install required pip modules
```
pipenv install --python 3.6
pipenv shell
pip install -r requirements.txt
```

## Running game
```
python game.py
```
